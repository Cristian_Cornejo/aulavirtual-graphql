'use strict'

const connectDb = require('./db')
const { ObjectID } = require('mongodb')

module.exports = {
  // courses
  createCourse: async (root, { input }) => {
    const defaults = {
      topic: ''
    }

    const newCourse = Object.assign(defaults, input)
    let db
    let course

    try {
      db = await connectDb()
      course = await db.collection('courses').insertOne(newCourse)
      newCourse._id = course.insertedId
    } catch (error) {
      console.error(error)
    }

    return newCourse
  },

  editCourse: async (root, { _id, input }) => {
    let db
    let course

    try {
      db = await connectDb()
      await db
        .collection('courses')
        .updateOne({ _id: ObjectID(_id) }, { $set: input })
      course = await db.collection('courses').findOne({ _id: ObjectID(_id) })
    } catch (error) {
      console.error(error)
    }

    return course
  },

  deleteCourse: async (root, { _id }) => {
    let db

    try {
      db = await connectDb()
      await db.collection('courses').deleteOne({ _id: ObjectID(_id) })
    } catch (error) {
      console.error(error)
    }

    return true
  },
  
  addStudent: async (root, { courseID, studentID }) => {
    let db
    let person
    let course

    try {
      db = await connectDb()
      course = await db.collection('courses').findOne({
        _id: ObjectID(courseID)
      })
      person = await db.collection('students').findOne({
        _id: ObjectID(studentID)
      })

      if (!course || !person) throw new Error('La Persona o el Curso no existe')

      await db
        .collection('courses')
        .updateOne(
          { _id: ObjectID(courseID) },
          { $addToSet: { students: ObjectID(studentID) } }
        )
    } catch (error) {
      console.error(error)
    }
    course = await db.collection('courses').findOne({
      _id: ObjectID(courseID)
    })
    return course
  },

  addTeacher: async (root, { courseID, teacherID }) => {
    let db
    let person
    let course

    try {
      db = await connectDb()
      course = await db.collection('courses').findOne({
        _id: ObjectID(courseID)
      })
      person = await db.collection('teachers').findOne({
        _id: ObjectID(teacherID)
      })

      if (!course || !person) throw new Error('La Persona o el Curso no existe')

      await db
        .collection('courses')
        .updateOne(
          { _id: ObjectID(courseID) },
          { $addToSet: { teachers: ObjectID(teacherID) } }
        )
    } catch (error) {
      console.error(error)
    }
    course = await db.collection('courses').findOne({
      _id: ObjectID(courseID)
    })
    return course
  },



  // students
  createStudent: async (root, { input }) => {
    let db
    let student

    try {
      db = await connectDb()
      student = await db.collection('students').insertOne(input)
      input._id = student.insertedId
    } catch (error) {
      console.error(error)
    }

    return input
  },

  editStudent: async (root, { _id, input }) => {
    let db
    let student

    try {
      db = await connectDb()
      await db
        .collection('students')
        .updateOne({ _id: ObjectID(_id) }, { $set: input })
      student = await db.collection('students').findOne({ _id: ObjectID(_id) })
    } catch (error) {
      console.error(error)
    }

    return student
  },

  deleteStudent: async (root, { _id }) => {
    let db

    try {
      db = await connectDb()
      await db.collection('student').deleteOne({
        _id: ObjectID(_id)
      })
    } catch (error) {
      console.error(error)
    }

    return true
  },

  // teachers

  createTeacher: async (root, { input }) => {
    let db
    let teacher

    try {
      db = await connectDb()
      teacher = await db.collection('teachers').insertOne(input)
      input._id = teacher.insertedId
    } catch (error) {
      console.error(error)
    }

    return input
  },

  editTeacher: async (root, { _id, input }) => {
    let db
    let teacher

    try {
      db = await connectDb()
      await db
        .collection('teachers')
        .updateOne({ _id: ObjectID(_id) }, { $set: input })
      teacher = await db.collection('teachers').findOne({ _id: ObjectID(_id) })
    } catch (error) {
      console.error(error)
    }

    return teacher
  },

  deleteTeacher: async (root, { _id }) => {
    let db

    try {
      db = await connectDb()
      await db.collection('teachers').deleteOne({
        _id: ObjectID(_id)
      })
    } catch (error) {
      console.error(error)
    }

    return true
  },

  // classroom
  createClassroom: async (root, { input }) => {
    let db
    let classroom

    try {
      db = await connectDb()
      classroom = await db.collection('classrooms').insertOne(input)
      input._id = classroom.insertedId
    } catch (error) {
      console.error(error)
    }

    return input
  },

  editClassroom: async (root, { _id, input }) => {
    let db
    let classroom

    try {
      db = await connectDb()
      await db
        .collection('classrooms')
        .updateOne({ _id: ObjectID(_id) }, { $set: input })
      classroom = await db.collection('classrooms').findOne({ _id: ObjectID(_id) })
    } catch (error) {
      console.error(error)
    }

    return classroom
  },

  deleteClassroom: async (root, { _id }) => {
    let db

    try {
      db = await connectDb()
      await db.collection('classrooms').deleteOne({
        _id: ObjectID(_id)
      })
    } catch (error) {
      console.error(error)
    }

    return true
  },
  addStudentToClassroom: async (root, {classroomID, studentID }) => {
    let db
    let person
    let classroom

    try {
      db = await connectDb()
      classroom = await db.collection('classrooms').findOne({
        _id: ObjectID(classroomID)
      })
      person = await db.collection('students').findOne({
        _id: ObjectID(studentID)
      })

      if (!classroom || !person) throw new Error('La Persona o el Curso no existe')

      await db
        .collection('classrooms')
        .updateOne(
          { _id: ObjectID(classroomID) },
          { $addToSet: { students: ObjectID(studentID) } }
        )
    } catch (error) {
      console.error(error)
    }
    classroom = await db.collection('classrooms').findOne({
      _id: ObjectID(classroomID)
    })
    return classroom
  },

}
