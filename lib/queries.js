'use strict'

const connectDb = require('./db')
const { ObjectID } = require('mongodb')

module.exports = {
  getCourses: async () => {
    let db
    let courses = []

    try {
      db = await connectDb()
      courses = await db
        .collection('courses')
        .find()
        .toArray()
    } catch (error) {
      console.error(error)
    }

    return courses
  },
  getCourse: async (root, { id }) => {
    let db
    let course

    try {
      db = await connectDb()
      course = await db.collection('courses').findOne({
        _id: ObjectID(id)
      })
    } catch (error) {
      console.error(error)
    }

    return course
  },
  getStudents: async () => {
    let db
    let students = []

    try {
      db = await connectDb()
      students = await db
        .collection('students')
        .find()
        .toArray()
    } catch (error) {
      console.error(error)
    }

    return students
  },
  getStudent: async (root, { id }) => {
    let db
    let student

    try {
      db = await connectDb()
      student = await db.collection('students').findOne({
        _id: ObjectID(id)
      })
    } catch (error) {
      console.error(error)
    }

    return student
  },
  // teachers
  getTeachers: async () => {
    let db
    let teachers = []

    try {
      db = await connectDb()
      teachers = await db
        .collection('teachers')
        .find()
        .toArray()
    } catch (error) {
      console.error(error)
    }

    return teachers
  },
  getTeacher: async (root, { id }) => {
    let db
    let teacher

    try {
      db = await connectDb()
      teacher = await db.collection('teachers').findOne({
        _id: ObjectID(id)
      })
    } catch (error) {
      console.error(error)
    }

    return teacher
  },
   // classroom
   getClassrooms: async () => {
    let db
    let classrooms = []

    try {
      db = await connectDb()
      classrooms = await db
        .collection('classrooms')
        .find()
        .toArray()
    } catch (error) {
      console.error(error)
    }

    return classrooms
  },
  getClassroom: async (root, { id }) => {
    let db
    let classroom

    try {
      db = await connectDb()
      classroom = await db.collection('classrooms').findOne({
        _id: ObjectID(id)
      })
    } catch (error) {
      console.error(error)
    }

    return classroom
  }
}
