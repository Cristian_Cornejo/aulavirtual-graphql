const { MongoClient } = require('mongodb')
const { DB_USER, DB_PASS, DB_URL, DB_NAME } = process.env

const mongoUrl = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_URL}/${DB_NAME}?retryWrites=true&w=majority`
let connection

async function connectDB () {
  if (connection) return connection

  let client
  try {
    client = await MongoClient.connect(mongoUrl, {
      useNewUrlParser: true
    })
    connection = client.db(DB_NAME)
  } catch (error) {
    console.error('No se pude conectar a la bd', mongoUrl, error)
    procces.exit(1)
  }

  return connection
}

module.exports = connectDB
