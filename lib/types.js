'use strict'

const connectDb = require('./db')
const { ObjectID } = require('mongodb')

module.exports = {
  Course: {
    students: async ({ students }) => {
      let db
      let studentsData
      let ids
      try {
        db = await connectDb()
        ids = students ? students.map(id => ObjectID(id)) : []
        studentsData =
          ids.length > 0
            ? await db
                .collection('students')
                .find({ _id: { $in: ids } })
                .toArray()
            : []
      } catch (error) {
        console.error(error)
      }

      return studentsData
    },
    teachers: async ({ teachers }) => {
      let db
      let teachersData
      let ids
      try {
        db = await connectDb()
        ids = teachers ? teachers.map(id => ObjectID(id)) : []
        teachersData =
          ids.length > 0
            ? await db
                .collection('teachers')
                .find({ _id: { $in: ids } })
                .toArray()
            : []
      } catch (error) {
        console.error(error)
      }
      return teachersData
    }
  },
  Classroom: {
    teacher: async ({ teacher }) => {
      let db
      let teacherRes

      try {
        db = await connectDb()
        teacherRes = await db.collection('teachers').findOne({
          _id: ObjectID(teacher)
        })
      } catch (error) {
        console.error(error)
      }
      return teacherRes
    },
    course: async ({ course }) => {
      let db
      let courseRes

      try {
        db = await connectDb()
        courseRes = await db.collection('courses').findOne({
          _id: ObjectID(course)
        })
      } catch (error) {
        console.error(error)
      }
      return courseRes
    },
    students: async ({ students }) => {
      let db
      let studentsData
      let ids
      try {
        db = await connectDb()
        ids = students ? students.map(id => ObjectID(id)) : []
        studentsData =
          ids.length > 0
            ? await db
                .collection('students')
                .find({ _id: { $in: ids } })
                .toArray()
            : []
      } catch (error) {
        console.error(error)
      }

      return studentsData
    }
  }
}
