# AULAVIRTUAL CON GRAPHQL

API REST realizada con GraphQL y MongoDB, permite realizar operaciones crud y realizar consultas dinamicas de 4 colecciones: estudiantes, profesores, curso, y salon de clases.

Con GraphiQL para observar documentación de cada query o mutacion y poder operar con estas.

## Scripts Disponibles

### `npm start`

Enciende la aplicación en el puerto [http://localhost:3000](http://localhost:3000)

### `npm run dev`

Enciende la aplicación en el puerto [http://localhost:3000](http://localhost:3000) con opciones para desarrollo.
